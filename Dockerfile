FROM continuumio/miniconda3
ENV PYTHONUNBUFFERED=1
RUN apt-get --allow-releaseinfo-change update && \
    apt-get upgrade -y && \
    apt-get install -y build-essential wget gcc && \
    apt-get install nano && \
    apt-get clean -y && \
    mkdir -p /home/app && \
    groupadd -r app && \
    useradd -r -g app -d /home/app -s /sbin/nologin -c "Docker image user" app && \
    conda init bash && \
    conda config --set ssl_verify false && \
    echo "source activate base" > ~/.bashrc
WORKDIR /home/app
COPY environment.yml /home/app/
RUN chown -R app:app /home/app
RUN conda env update -n base -f environment.yml
RUN pip install torch-scatter -f https://data.pyg.org/whl/torch-1.9.0+cpu.html && \
    pip install torch-sparse -f https://data.pyg.org/whl/torch-1.9.0+cpu.html && \
    pip install torch-geometric && \
    pip install torch-cluster -f https://data.pyg.org/whl/torch-1.9.0+cpu.html && \
    pip install torch-spline-conv -f https://data.pyg.org/whl/torch-1.9.0+cpu.html
COPY sentence2graph_gnn /home/app/sentence2graph_gnn/
