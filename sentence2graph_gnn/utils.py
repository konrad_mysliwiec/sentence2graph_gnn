import os
import pickle
import torch


def create_directory(directory: str):
    if not os.path.exists(directory):
        os.makedirs(directory)


def save_to_pickle(obj, file_path: str):
    with open(file_path, 'wb') as f:
        pickle.dump(obj, f)


def load_from_pickle(file_path: str):
    with open(file_path, 'rb') as f:
        return pickle.load(f)


def _to_undirected(edge_index: torch.Tensor) -> torch.Tensor:
    return torch.cat(
        [
            edge_index,
            reverse_edges(edge_index)
        ],
        axis=1
    )


def reverse_edges(edge_index: torch.Tensor) -> torch.Tensor:
    if edge_index.shape[1] == 2:
        return torch.tensor(
            [
                edge_index[:, 1].tolist(),
                edge_index[:, 0].tolist()
            ]
        ).T
    elif edge_index.shape[0] == 2:
        return torch.tensor(
            [
                edge_index[1].tolist(),
                edge_index[0].tolist()
            ]
        )
