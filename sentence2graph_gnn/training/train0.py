from tensorboardX import SummaryWriter
from sentence2graph_gnn.sentence_dataset_split_separate_graphs import \
    SemMedSentenceSplitDataset, train_val_test_split_graph_data
from sentence2graph_gnn.models.train import train
from sentence2graph_gnn.models.model_args import ModelArgs
from datetime import datetime
import os.path as osp
from sentence2graph_gnn.utils import load_from_pickle
import sentence2graph_gnn.settings as settings
from sentence2graph_gnn.models.node_class_model import \
    NodeClassificationRGCNNetwork


# Load number of tags and dependencies
dep_encoder = load_from_pickle(
    osp.join(settings.DEPENDENCIES_LABEL_ENCODER_PATH)
)
tag_encoder = load_from_pickle(
    osp.join(settings.TAGS_LABEL_ENCODER_PATH)
)

num_relations = dep_encoder.classes_.shape[0]
num_nodes = tag_encoder.classes_.shape[0]

# Load dataset
dataset_dir = osp.join(
    'sentence2graph_gnn',
    'datasets',
    'semmed_sentence_split_dataset_simple'
)
dataset = SemMedSentenceSplitDataset(root=dataset_dir, graphs_data=None)

# Load model metadata
args = ModelArgs()
args.num_layers = 2
args.hidden_dim = 50
args.epochs = 100
args.lr = 0.001
args.model_name = 'GCN'
args.aggr = 'add'
args.dropout = 0.0
args.opt = 'adam'

model_name = f'split_separate_graphs_num_blocks_10_{args.model_name}' \
    f'_dropout_{args.dropout}' \
    f'_hid_dim_{args.hidden_dim}_epochs_{args.epochs}_lr_{args.lr}' \
    f'_weight_decay_{args.weight_decay}_opt_{args.opt}_aggr_{args.aggr}' \
    f'_num_lay_{args.num_layers}'
writer = SummaryWriter(
    f"models_logs/log/{model_name}" + datetime.now().strftime("%Y%m%d-%H%M%S")
)
checkpoint_dir = f'checkpoints/basic/{model_name}'

# Load model
model = NodeClassificationRGCNNetwork(
    num_nodes=num_nodes,
    hidden_channels=args.hidden_dim,
    num_relations=num_relations,
    args=args,
    output_dim=4,
    nodes_types=dataset.data.nodes_types,
    num_blocks=10
)

# Run train loop
train(
    model=model,
    dataset=dataset,
    args=args,
    writer=writer,
    checkpoint_dir=checkpoint_dir
)