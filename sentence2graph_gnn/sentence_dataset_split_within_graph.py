import os.path as osp
from typing import Callable, List, Optional, Tuple, Union
from torch_geometric.data import Data, InMemoryDataset
import torch_geometric as pyg
import torch
import numpy as np
import random
from tqdm import tqdm

import sentence2graph_gnn.settings as settings
from sentence2graph_gnn.utils import create_directory, load_from_pickle, \
    _to_undirected


def get_sentence_data(
    graph_data: dict,
    val_ratio=0.2,
    test_ratio=0.1
) -> pyg.data.Data:
    """Returns Data object for graphs composed from SemMed sentence.

    Args:
        graph_data (str): Dictionary with graph data.
            Graph data has to be an output from
            'parse_tree_with_edge_type_encoded' function.
        val_ratio (float, optional): Validation ratio for a dataset.
            Defaults to 0.2.
        test_ratio (float, optional): Test ratio for a dataset.
            Defaults to 0.1.

    Returns:
        pyg.data.Dataset: Dataset for DDI network.
    """
    edge_index = torch.tensor(
        graph_data['edge_list'],
        dtype=torch.long
    ).T
    edge_index = _to_undirected(edge_index)
    edge_type = torch.tensor(graph_data['edge_type'], dtype=torch.long)
    edge_type = torch.cat([edge_type, edge_type])
    nodes_types = torch.tensor(graph_data['nodes_features'], dtype=torch.long)
    y = torch.tensor(graph_data['nodes_labels'], dtype=torch.long)
    num_classes = torch.unique(y).size(0)
    num_nodes = len(y)  # edge_index.max()+1
    nodes_ids = np.arange(num_nodes)
    dataset_masks = creates_masks(
        nodes_ids,
        val_ratio,
        test_ratio
    )
    # TODO: Check if num_classes shouldn't be a list - [num_classes]
    data = Data(
        nodes_types=nodes_types,
        edge_index=edge_index,
        num_nodes=num_nodes,
        edge_type=edge_type,
        num_relations=edge_type.unique().shape[0],
        y=y,
        num_classes=num_classes,
        test_mask=dataset_masks['test'],
        train_mask=dataset_masks['train'],
        val_mask=dataset_masks['validation']
    )

    return data


# TODO: Create masks more efficiently. Just shuffle list and take offset.
def creates_masks(nodes_list: List, val_ratio: float, test_ratio: float):
    """Creates a random mask for train, validation and test data.

    Args:
        nodes_list (List): List of nodes to create masks from.
        val_ratio (float): Validation ratio.
        test_ratio (float): Test ratio.

    Returns:
        [Dict[str, torch.tensor]]: A dictionary with split data.
            The keys are: 'train', 'validation', 'test' and the values are
            tensors with boolean values which indicates if a given node is
            part of a specific dataset.
    """
    num_nodes = len(nodes_list)
    val_len = int(num_nodes * val_ratio)
    test_len = int(num_nodes * test_ratio)
    train_len = num_nodes - val_len - test_len
    if num_nodes <= 4:
        return {
            "validation": torch.zeros(num_nodes, dtype=torch.bool),
            "test": torch.zeros(num_nodes, dtype=torch.bool),
            "train": torch.tensor(nodes_list)
        }
    if val_len == 0:
        val_len += 1
        train_len -= 1
    if test_len == 0:
        test_len += 1
        train_len -= 1
    len_dictionary = {
        "validation": val_len,
        "test": test_len,
        "train": train_len
    }
    datasets = {}
    labels_set = set(nodes_list)

    for dataset_type in len_dictionary.keys():
        temp_list = list(labels_set)
        sampling = random.sample(temp_list, k=len_dictionary[dataset_type])
        datasets[dataset_type] = torch.tensor(
            np.isin(nodes_list, sampling),
            dtype=torch.bool
        )
        labels_set.difference_update(set(sampling))

    return datasets


class SemMedSentenceDataset(InMemoryDataset):
    def __init__(
        self,
        root: Optional[str],
        graphs_data: list,
        transform: Optional[Callable] = None,
        pre_transform: Optional[Callable] = None,
        pre_filter: Optional[Callable] = None,
        test_ratio: float = 0.1,
        val_ratio: float = 0.2
    ):
        self.val_ratio = val_ratio
        self.test_ratio = test_ratio
        self.graphs_data = graphs_data
        super(SemMedSentenceDataset, self).__init__(
            root=root,
            transform=transform,
            pre_transform=pre_transform,
            pre_filter=pre_filter,
        )
        self.data, self.slices = torch.load(self.processed_paths[0])
        create_directory(root)

    @property
    def raw_file_names(self) -> Union[str, List[str], Tuple]:
        return []

    @property
    def processed_dir(self) -> str:
        return osp.join(self.root, 'processed')

    @property
    def processed_file_names(self) -> Union[str, List[str], Tuple]:
        return ['data_0.pt']

    def download(self):
        pass

    def process(self):

        data_list = [
            get_sentence_data(
                graph_data,
                self.val_ratio,
                self.test_ratio
            )
            for graph_data in tqdm(self.graphs_data[:3480])
            if len(graph_data['edge_list']) > 2
        ]

        data, slices = self.collate(data_list)
        torch.save((data, slices), self.processed_paths[0])


if __name__ == '__main__':
    graphs_data = load_from_pickle(settings.GRAPH_DATA_PATH)
    root = osp.join('datasets', 'semmed_sentence_dataset_simple')
    dataset = SemMedSentenceDataset(root=root, graphs_data=graphs_data)
