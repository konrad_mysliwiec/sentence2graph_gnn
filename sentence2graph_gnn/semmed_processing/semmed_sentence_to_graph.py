import numpy as np
import os
import pandas as pd
from tqdm import tqdm

import sentence2graph_gnn.settings as settings
from sentence2graph_gnn.sentence2graph.sentence_to_graph import \
    get_nodes_features_labels, parse_tree_with_edge_type_encoded
from sentence2graph_gnn.utils import load_from_pickle, save_to_pickle


def load_semmed_graph_data(directory: str) -> list:
    """Loads semmed graph data.

    Graph data object is a dictionary in the following format:
        {
            "nodes_features": list,
            "edge_list": list,
            "edge_type": list,
            "nodes_labels": list
        }
    For example:
        {
            "nodes_features": [1, 3, 2],
            "edge_list": [[1, 3], [3, 2]]
            "edge_type": [14, 27],
            "nodes_labels": [14, 17, 40]
        }
    , where:
        - "nodes_features" is a list of label encoded tags of words
            (nodes' features)
        - "edge_list" is an edge index of the created graph
        - "edge_type" is a list of label encoded dependencies
            (relations' types)
        - "nodes_labels" is a list of nodes' types: '1' means subject,
            '2' means object, '3' means predicate and '0' means none of
            mentioned types

    Args:
        directory (str): Location to store graphs data.
            If provided directory doesn't exists, the graph data
            will be created and saved in the provided directory.

    Returns:
        list: List of graph data in the following format:
            {
            "nodes_features": list,
            "edge_list": list,
            "edge_type": list,
            "nodes_labels": list
        }
    """
    if not os.path.exists(directory):
        return parse_semmed_to_graph(directory)
    else:
        return load_from_pickle(directory)


def parse_semmed_to_graph(
    output_dir: str = None
) -> list:
    """Parses semmed data into list of graph data.

    Graph data object is a dictionary in the following format:
        {
            "nodes_features": list,
            "edge_list": list,
            "edge_type": list,
            "nodes_labels": list
        }
    For example:
        {
            "nodes_features": [1, 3, 2],
            "edge_list": [[1, 3], [3, 2]]
            "edge_type": [14, 27],
            "nodes_labels": [14, 17, 40]
        }
    , where:
        - "nodes_features" is a list of label encoded tags of words
            (nodes' features)
        - "edge_list" is an edge index of the created graph
        - "edge_type" is a list of label encoded dependencies
            (relations' types)
        - "nodes_labels" is a list of nodes' types: '1' means subject,
            '2' means object, '3' means predicate and '0' means none of
            mentioned types

    Args:
        output_dir (str, optional): Location to store graphs data.
            If not provided the data are not saved.
            Defaults to None.

    Returns:
        list: List of graph data in the following format:
            {
            "nodes_features": list,
            "edge_list": list,
            "edge_type": list,
            "nodes_labels": list
        }
    """
    svo_df = pd.read_pickle(settings.SVO_SEMMED_DATA_PATH)
    graphs_data = []
    for sentence_data in tqdm(svo_df.to_numpy(), desc="Graph data progress"):
        sentence = sentence_data[0]
        sub_id = sentence_data[1]
        obj_id = sentence_data[2]
        pred_id = sentence_data[3]
        nodes_features = get_nodes_features_labels(sentence)
        edge_data = parse_tree_with_edge_type_encoded(sentence)
        edge_list = [[edge[0], edge[1]] for edge in edge_data]
        edge_type = [edge[2] for edge in edge_data]
        nodes_labels = np.zeros(len(nodes_features))
        nodes_labels[sub_id] = 1
        nodes_labels[obj_id] = 2
        nodes_labels[pred_id] = 3
        graphs_data.append({
            "nodes_features": nodes_features,
            "edge_list": edge_list,
            "edge_type": edge_type,
            "nodes_labels": nodes_labels
        })
    if output_dir:
        save_to_pickle(graphs_data, output_dir)
    return graphs_data


if __name__ == '__main__':
    parse_semmed_to_graph(settings.GRAPH_DATA_PATH)
