import pandas as pd
import sentence2graph_gnn.settings as settings
import spacy


nlp = spacy.load("en_core_web_md")


def svo_semmed_processing():
    """Parses clean SemMed df into a dataframe with Subject, Predicate, Object.

    The clean dataframe is a dataframe which has properly alligned
    start indices for subject, predicate and object. The output
    dataframe contains the following columns:
        - 'SECTION_HEADER' - with the actual sentence text
        - 'SUBJECT_ID' - list with sentence's subjects ids
        - 'OBJECT_ID' - list with sentence's objects ids
        - 'PREDICATION_ID' - list with sentence's predications ids
    Ids in this case are the sentence token ids which serve as ids
    in a graph.

    This function saves the dataframe and is part of ETL process.
    """
    data_df = pd.read_csv(settings.CLEAN_SEMMED_DATA_PATH, sep='|')
    # Get subject, object and predicate id
    data_df['SUBJECT_ID'] = data_df[
        ['SUBJ_SENT_START_INDEX', 'SECTION_HEADER']
    ].apply(lambda x: _get_word_id_from_df(x, 'SUBJ_SENT_START_INDEX'), axis=1)
    data_df['OBJECT_ID'] = data_df[
        ['OBJ_SENT_START_INDEX', 'SECTION_HEADER']
    ].apply(lambda x: _get_word_id_from_df(x, 'OBJ_SENT_START_INDEX'), axis=1)
    data_df['PREDICATION_ID'] = data_df[
        ['PRED_SENT_START_INDEX', 'SECTION_HEADER']
    ].apply(lambda x: _get_word_id_from_df(x, 'PRED_SENT_START_INDEX'), axis=1)
    svo_df = data_df[
        ['SECTION_HEADER', 'SUBJECT_ID', 'OBJECT_ID', 'PREDICATION_ID']
    ]
    # Filter out words which plays both subject and object
    svo_df['VALID_INDEX'] = svo_df.apply(
        lambda x: _filter_labels_with_the_same_index(x),
        axis=1
    )
    svo_df = svo_df[svo_df['VALID_INDEX'] == True]
    svo_df.dropna(inplace=True)
    svo_df.reset_index(inplace=True)
    svo_df.drop(columns='index', inplace=True)
    # Cast ID columns to int
    svo_df = svo_df.astype({
        "SUBJECT_ID": int,
        "OBJECT_ID": int,
        "PREDICATION_ID": int
    })
    svo_df.reset_index(inplace=True)
    svo_df.drop(columns='index', inplace=True)
    svo_df.drop(columns='VALID_INDEX', inplace=True)
    # Aggregate all subject, objects and predicates for a given sentence
    svo_df = svo_df.groupby('SECTION_HEADER').agg(lambda x: list(x))
    svo_df.reset_index(inplace=True)
    svo_df.to_pickle(settings.SVO_SEMMED_DATA_PATH)


def _get_word_id_from_df(df: pd.DataFrame, word_column: str) -> int:
    """Returns token id from SemMed dataframe row for a word in a given column.

    Args:
        df (pd.DataFrame): SemMed clean dataframe row with 'SECTION_HEADER'
            column and a column with a word which token id should be returned
            for.
        word_column (str): Name of the column with a word which token should be
            derived from. This column name should be in 'df' column names.

    Returns:
        int: Token id for a word from a given word column for a sentence
            in a given SemMed dataframe row.
    """
    sentence = df['SECTION_HEADER']
    word_start_ind = df[word_column]
    return _get_word_id(word_start_ind, sentence)


def _get_word_id(word_start_ind: int, sentence: str) -> int:
    """Returns token id in a given sentence.

    Args:
        word_start_ind (int): Start index of a token which token id should be
            returned for.
        sentence (str): Sentence with a given token, which token id should be
            derived from

    Returns:
        int: Token id for a given start index and a given sentence.
    """
    doc = nlp(sentence)
    i = 0
    for token in doc:
        if i == word_start_ind:
            return(token.i)
        i += len(token.text_with_ws)


def _filter_labels_with_the_same_index(df: pd.DataFrame) -> bool:
    """Checks one word plays two roles in a given SemMed dataframe row.

    The roles are: subject, predicate or object. If one word plays
        at least two of these roles then returns True. Otherwise False.

    Args:
        df (pd.DataFrame): SemMed dataframe row with 'SUBJECT_ID', 'OBJECT_ID'.
            'PREDICATION_ID'.

    Returns:
        bool: Returns True if one word plays at least roles. Otherwise False.
    """
    subject_index = df['SUBJECT_ID']
    object_index = df['OBJECT_ID']
    predicate_index = df['PREDICATION_ID']
    if subject_index == object_index or \
            subject_index == predicate_index or \
            object_index == predicate_index:
        return False
    else:
        return True


if __name__ == '__main__':
    svo_semmed_processing()
