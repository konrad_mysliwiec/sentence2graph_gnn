import pandas as pd
import re
from itertools import product

import sentence2graph_gnn.settings as settings


def clean_semmed(
    pred_nrows: int = 200000,
    pred_aux_nrows: int = 200000,
    sentence_nrows: int = 9000000
):
    """Merges SemMed tables for predications and sentence data.

    Despite of merging required tables it also derives the raw text and
        it's start index for: subjects, predicates and objects.
    Merged tables are:
        - 'PREDICATION'
        - 'PREDICATION_AUX'
        - "SENTENCE'
    The output dataset is saved to csv and contains the following tables:
        - 'SUBJECT_TEXT'
        - 'SUBJ_SENT_START_INDEX'
        - 'OBJECT_TEXT'
        - 'OBJ_SENT_START_INDEX'
        - 'PREDICATION_TEXT'
        - 'PRED_SENT_START_INDEX'
        - 'SECTION_HEADER'


    Args:
        pred_nrows (int, optional): Number of rows to load
            from 'PREDICATION' table. Defaults to 200000.
        pred_aux_nrows (int, optional): Number of rows to load
            from 'PREDICATION_AUX' table. Defaults to 200000.
        sentence_nrows (int, optional): Number of rows to load
            from 'SENTENCE' table. Defaults to 9000000.
    """
    # Load PREDICATION
    pred_columns = [
        'PREDICATION_ID', 'SENTENCE_ID', 'PMID', 'PREDICATE', 'SUBJECT_CUI',
        'SUBJECT_NAME', 'SUBJECT_SEMTYPE', 'SUBJECT_NOVELTY', 'OBJECT_CUI',
        'OBJECT_NAME', 'OBJECT_SEMTYPE', 'OBJECT_NOVELTY'
    ]
    pred = pd.read_csv(
        settings.RAW_SEMMED_PREDICATION_DATA_PATH,
        nrows=pred_nrows,
        header=None
    )
    pred.drop(columns=[12, 13, 14], inplace=True)
    pred.columns = pred_columns
    # Load PREDICATION_AUX
    pred_aux_columns = [
        'PREDICATION_AUX_ID', 'PREDICATION_ID', 'SUBJECT_TEXT', 'SUBJECT_DIST',
        'SUBJECT_MAXDIST', 'SUBJECT_START_INDEX', 'SUBJECT_END_INDEX',
        'SUBJECT_SCORE', 'INDICATOR_TYPE', 'PREDICATE_START_INDEX',
        'PREDICATE_END_INDEX', 'OBJECT_TEXT', 'OBJECT_DIST', 'OBJECT_MAXDIST',
        'OBJECT_START_INDEX', 'OBJECT_END_INDEX', 'OBJECT_SCORE',
        'CURR_TIMESTAMP'
    ]
    pred_aux = pd.read_csv(
        settings.RAW_SEMMED_PREDICATION_AUX_DATA_PATH,
        nrows=pred_aux_nrows,
        header=None
    )
    pred_aux.columns = pred_aux_columns
    # Load SENTENCE
    sent_columns = [
        'SENTENCE_ID', 'PMID', 'TYPE', 'NUMBER', 'SENTENCE', 'SECTION_HEADER',
        'NORMALIZED_SECTION_HEADER'
    ]
    sent = pd.read_csv(
        settings.RAW_SEMMED_SENTENCE_DATA_PATH,
        nrows=sentence_nrows,
        usecols=list(range(7)),
        header=None
    )
    sent.columns = sent_columns
    # Merge
    pred_merged = pred.merge(
        pred_aux, left_on=['PREDICATION_ID'], right_on=['PREDICATION_ID']
    )
    pred_merged = pred_merged.merge(
        sent, left_on=['SENTENCE_ID'], right_on=['SENTENCE_ID']
    )
    pred_merged = pred_merged[
        [
            'PMID_x', 'PMID_y', 'SUBJECT_TEXT', 'SUBJECT_NAME',
            'SUBJECT_START_INDEX', 'SUBJECT_END_INDEX', 'PREDICATE',
            'PREDICATE_START_INDEX', 'PREDICATE_END_INDEX', 'OBJECT_TEXT',
            'OBJECT_NAME', 'OBJECT_START_INDEX', 'OBJECT_END_INDEX',
            'NORMALIZED_SECTION_HEADER', 'SECTION_HEADER'
        ]
    ]
    # Find the correct offset for subject, predicate and object
    pred_merged['offset'] = pred_merged.apply(
        lambda x: _find_offset(x), axis=1
    )
    pred_merged = pred_merged.dropna(subset=['offset'])
    pred_merged = pred_merged.astype({'offset': int})
    pred_merged['SUBJ_SENT_START_INDEX'] = pred_merged['SUBJECT_START_INDEX'] \
        - pred_merged['offset']
    pred_merged['OBJ_SENT_START_INDEX'] = pred_merged['OBJECT_START_INDEX'] \
        - pred_merged['offset']
    pred_merged['PRED_SENT_START_INDEX'] = \
        pred_merged['PREDICATE_START_INDEX'] - pred_merged['offset']
    # Filter data to get only these rows for which subject, object
    # and predication are singular words
    subj_sing_word_mask = pred_merged.SUBJECT_TEXT.\
        apply(lambda x: len(x.split())) == 1
    pred_merged = pred_merged[subj_sing_word_mask]

    obj_sing_word_mask = pred_merged.OBJECT_TEXT.\
        apply(lambda x: len(x.split())) == 1
    pred_merged = pred_merged[obj_sing_word_mask]

    pred_merged['IS_PREDICATE_VALID'] = pred_merged.apply(
        _validate_predicate_index, axis=1
    )
    pred_merged = pred_merged[pred_merged['IS_PREDICATE_VALID']]
    pred_merged['PRED_SENT_END_INDEX'] = \
        pred_merged['PRED_SENT_START_INDEX'] \
        + pred_merged['PREDICATE_END_INDEX'] \
        - pred_merged['PREDICATE_START_INDEX']
    pred_merged['PREDICATION_TEXT'] = pred_merged[
        ['SECTION_HEADER', 'PRED_SENT_START_INDEX', 'PRED_SENT_END_INDEX']
    ].apply(
        lambda x: _get_word_from_header(
            x, 'PRED_SENT_START_INDEX', 'PRED_SENT_END_INDEX'
        ),
        axis=1
    )
    pred_sing_word_mask = pred_merged.PREDICATION_TEXT.\
        apply(lambda x: len(x.split())) == 1
    pred_merged = pred_merged[pred_sing_word_mask]
    pred_merged = pred_merged[
        [
            'SUBJECT_TEXT', 'SUBJ_SENT_START_INDEX', 'OBJECT_TEXT',
            'OBJ_SENT_START_INDEX', 'PREDICATION_TEXT',
            'PRED_SENT_START_INDEX', 'SECTION_HEADER'
        ]
    ]
    pred_merged.to_csv(
        settings.CLEAN_SEMMED_DATA_PATH,
        index=False,
        sep='|'
    )


def _find_offset(df: pd.DataFrame) -> int:
    """Returns the offset between provided entity index and the actual one.

    SemMed dataset has subject, predicate and object indices starting from
        the beginning of whole article rather than a given sentence. This
        function tries to find the offset between indices from SemMed dataset
        and the indices from the provided sentences. It runs on a dataframe
        for merged and processed SemMed dataset which contains
        the following columns:
            - 'SUBJECT_TEXT'
            - 'OBJECT_TEXT'
            - 'SECTION_HEADER'
            - 'SUBJECT_START_INDEX'
            - ' OBJECT_START_INDEX'

    Args:
        df (pd.DataFrame): Dataframe for SemMed dataset row.

    Returns:
        int: Offset between start index provided by SemMed and
            the start index of an entity in a sentence that it appears.
    """
    offset = None
    subject_index_start = list(
        re.finditer(
            re.escape(str(df['SUBJECT_TEXT'])),
            df['SECTION_HEADER']
        )
    )
    object_index_start = list(
        re.finditer(
            re.escape(str(df['OBJECT_TEXT'])),
            df['SECTION_HEADER']
        )
    )
    # Subject or Object not found
    if len(subject_index_start) == 0 or len(object_index_start) == 0:
        return None
    # Only one subject candidate found - offset found
    if len(subject_index_start) == 1:
        offset = int(df['SUBJECT_START_INDEX']) \
            - subject_index_start[0].start()
    # Only one object candidate found - offset found
    elif len(object_index_start) == 1:
        offset = int(df['OBJECT_START_INDEX']) \
            - object_index_start[0].start()
    # Create product of subject and object indices
    # Subject offset should be equal to Object offset
    else:
        subject_index_start = [match.start() for match in subject_index_start]
        object_index_start = [match.start() for match in object_index_start]
        for sub_ind, obj_ind in \
                product(subject_index_start, object_index_start):
            obj_offset = int(df['OBJECT_START_INDEX']) - obj_ind
            subj_offset = int(df['SUBJECT_START_INDEX']) - sub_ind
            if obj_offset == subj_offset:
                offset = obj_offset
                break
    return offset


def _validate_predicate_index(df: pd.DataFrame) -> bool:
    """Validates the indices of a dataframe row for SemMed dataset.

    Validation checks if predication index is within the length of the sentence
        and checks if predication is neither subject nor object.
        Provided dataframe has to contain the following columns:
            - 'SECTION_HEADER'
            - 'PRED_SENT_START_INDEX'
            - 'OBJ_SENT_START_INDEX'
            - 'SUBJ_SENT_START_INDEX'

    Args:
        df (pd.DataFrame): Dataframe for SemMed dataset row.

    Returns:
        bool: True if validation for a give sentence runs succesfully,
            otherwise False.
    """
    if len(df['SECTION_HEADER']) < df['PRED_SENT_START_INDEX']:
        return False
    elif df['PRED_SENT_START_INDEX'] == df['OBJ_SENT_START_INDEX'] \
            or df['PRED_SENT_START_INDEX'] == df['SUBJ_SENT_START_INDEX']:
        return False
    else:
        return True


def _get_word_from_header(
    df: pd.DataFrame,
    word_start_index_column: str,
    word_end_index_column: str
) -> str:
    """Returns start and end indices for the actual word from a given sentence.

    Provided dataframe has to contain 'SECTION_HEADER' column
        which contains the whole sentence.

    Args:
        df (pd.DataFrame): Row from dataframe for SemMed dataset, which
            word should be derived from.
        word_start_index_column (str): Start index of a word to be derived.
        word_end_index_column (str): End index of a word to be derived.

    Returns:
        str: The actual word derived from a sentence
            for a given start and end index.
    """
    return df['SECTION_HEADER'][
        df[word_start_index_column]:df[word_end_index_column]
    ]


if __name__ == '__main__':
    clean_semmed()
