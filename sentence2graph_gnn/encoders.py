import os.path as osp
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from typing import Union

from sentence2graph_gnn.data.tags_and_deps import dependencies, tags
import sentence2graph_gnn.settings as settings
from sentence2graph_gnn.utils import create_directory, load_from_pickle, \
    save_to_pickle


def get_dependencies_encoder(
    encoder_type: str = "label_encoder",
    force_recreate: bool = False
) -> Union[LabelEncoder, OneHotEncoder]:
    """Returns sklearn encoder for language dependencies.
    Possible encoders to return:
        - LabelEncoder
        - OneHotEncoder

    Args:
        encoder_type (str, optional): If equals to:
            - "label_encoder" then returns sklearn.preprocessing.LabelEncoder.
            - "one_hot_encoder" then returns
                sklearn.preprocessing.OneHotEncoder.
            Defaults to "label_encoder".
        force_recreate (bool, optional): If True recreates already existing
            encoder. Defaults to False.

    Returns:
        Union[LabelEncoder, OneHotEncoder]: Encoder for language dependencies.
    """
    if encoder_type == "label_encoder":
        encoder_dir = settings.DEPENDENCIES_LABEL_ENCODER_PATH
        encoder_func = _label_encode
    elif encoder_type == "one_hot_encoder":
        encoder_dir = settings.DEPENDENCIES_ONE_HOT_ENCODER_PATH
        encoder_func = _one_hot_encode

    if not osp.exists(encoder_dir) or force_recreate:
        deps_list = list(dependencies.keys())
        return encoder_func(deps_list, encoder_dir)
    else:
        return load_from_pickle(encoder_dir)


def get_tags_encoder(
    encoder_type: str = "label_encoder",
    force_recreate: bool = False
):
    """Returns sklearn encoder for language tags.
    Possible encoders to return:
        - LabelEncoder
        - OneHotEncoder

    Args:
        encoder_type (str, optional): If equals to:
            - "label_encoder" then returns sklearn.preprocessing.LabelEncoder.
            - "one_hot_encoder" then returns
                sklearn.preprocessing.OneHotEncoder.
            Defaults to "label_encoder".
        force_recreate (bool, optional): If True recreates already existing
            encoder. Defaults to False.

    Returns:
        Union[LabelEncoder, OneHotEncoder]: Encoder for language tags.
    """
    if encoder_type == "label_encoder":
        encoder_dir = settings.TAGS_LABEL_ENCODER_PATH
        encoder_func = _label_encode
    elif encoder_type == "one_hot_encoder":
        encoder_dir = settings.TAGS_ONE_HOT_ENCODER_PATH
        encoder_func = _one_hot_encode

    if not osp.exists(encoder_dir) or force_recreate:
        tags_list = list(tags.keys())
        return encoder_func(tags_list, encoder_dir)
    else:
        return load_from_pickle(encoder_dir)


def _one_hot_encode(data: list, output_dir: str = None) -> OneHotEncoder:
    """Returns One Hot encoder trained on a given data.

    Args:
        data (list): Data to train encoder on.
        output_dir (str, optional): Directory to save the encoder in.
            Defaults to None.

    Returns:
        OneHotEncoder: OneHot Encoder trained on a provided data.
    """
    ohe = OneHotEncoder()
    ohe.fit(data)
    if output_dir:
        create_directory(osp.dirname(output_dir))
        save_to_pickle(ohe, output_dir)
    return ohe


def _label_encode(data: list, output_dir: str = None) -> LabelEncoder:
    """Returns Label encoder trained on a given data.

    Args:
        data (list): Data to train encoder on.
        output_dir (str, optional): Directory to save the encoder in.
            Defaults to None.

    Returns:
        LabelEncoder: Label Encoder trained on a provided data.
    """
    le = LabelEncoder()
    le.fit(data)
    if output_dir:
        create_directory(osp.dirname(output_dir))
        save_to_pickle(le, output_dir)
    return le
