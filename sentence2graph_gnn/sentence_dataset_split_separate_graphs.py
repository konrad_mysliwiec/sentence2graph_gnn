import os.path as osp
from typing import Callable, List, Optional, Tuple, Union
from torch_geometric.data import Data, InMemoryDataset
import torch_geometric as pyg
import torch
import random
from tqdm import tqdm

from sentence2graph_gnn.utils import create_directory, load_from_pickle, \
    _to_undirected
import sentence2graph_gnn.settings as settings


def get_sentence_data(
    graph_data: dict,
    dataset_type: str = 'train'
) -> pyg.data.Data:
    """Returns Data object for graphs composed from SemMed sentence.

    Args:
        graph_data (str): Dictionary with graph data.
            Graph data has to be an output from
            'parse_tree_with_edge_type_encoded' function.
        dataset_type (str): Type of the dataset to return. Possible types:
            - 'train'
            - 'val'
            - 'test'

    Returns:
        pyg.data.Dataset: Dataset for DDI network.
    """
    edge_index = torch.tensor(
        graph_data['edge_list'],
        dtype=torch.long
    ).T
    edge_index = _to_undirected(edge_index)
    edge_type = torch.tensor(graph_data['edge_type'], dtype=torch.long)
    edge_type = torch.cat([edge_type, edge_type])
    nodes_types = torch.tensor(graph_data['nodes_features'], dtype=torch.long)
    y = torch.tensor(graph_data['nodes_labels'], dtype=torch.long)
    num_classes = torch.unique(y).size(0)
    num_nodes = len(y)

    if dataset_type == 'train':
        is_train = True
        is_val = False
        is_test = False
    elif dataset_type == 'val':
        is_train = False
        is_val = True
        is_test = False
    else:
        is_train = False
        is_val = False
        is_test = True

    data = Data(
        nodes_types=nodes_types,
        edge_index=edge_index,
        num_nodes=num_nodes,
        edge_type=edge_type,
        num_relations=edge_type.unique().shape[0],
        y=y,
        num_classes=num_classes,
        is_train=is_train,
        is_val=is_val,
        is_test=is_test
    )

    return data


def train_val_test_split_graph_data(
    graph_data: list,
    val_ratio: float,
    test_ratio: float,
    shuffle: bool = False
) -> dict:
    """Splits given graph data to train val test datasets.

    Args:
        graph_data (list): Dictionary with graph data
            (loaded by 'load_semmed_graph_data' function).
        val_ratio (float): Percentage of validation data.
        test_ratio (float): Percentage of test data.
        shuffle (bool): If true data will be shuffle, otherwise data stay in
            the original order.

    Returns:
        dict: Dictionary with split graph data into train, valm test datasets.
            Output schema:
                {
                    "train": 'train_graph_data',
                    "val": 'train_graph_data',
                    "test": 'train_graph_data'
                }
    """
    if shuffle:
        graph_data = random.shuffle(graph_data)
    graph_data_val = graph_data[:int(len(graph_data)*val_ratio)]
    graph_data_test = graph_data[
        int(len(graph_data)*val_ratio):
        int(len(graph_data)*val_ratio)+int(len(graph_data)*test_ratio)
    ]
    graph_data_train = graph_data[
        int(len(graph_data)*val_ratio)+int(len(graph_data)*test_ratio):
    ]
    graph_data = {
        "train": graph_data_train,
        "val": graph_data_val,
        "test": graph_data_test
    }
    return graph_data


class SemMedSentenceSplitDataset(InMemoryDataset):
    def __init__(
        self,
        root: Optional[str],
        graphs_data: list,
        transform: Optional[Callable] = None,
        pre_transform: Optional[Callable] = None,
        pre_filter: Optional[Callable] = None,
        test_ratio: float = 0.1,
        val_ratio: float = 0.2
    ):
        self.val_ratio = val_ratio
        self.test_ratio = test_ratio
        self.graphs_data = graphs_data
        super(SemMedSentenceSplitDataset, self).__init__(
            root=root,
            transform=transform,
            pre_transform=pre_transform,
            pre_filter=pre_filter,
        )
        self.data, self.slices = torch.load(self.processed_paths[0])

        create_directory(root)

    @property
    def raw_file_names(self) -> Union[str, List[str], Tuple]:
        return []

    @property
    def processed_dir(self) -> str:
        return osp.join(self.root, 'processed')

    @property
    def processed_file_names(self) -> Union[str, List[str], Tuple]:
        return ['data_0.pt']

    def download(self):
        pass

    def process(self):

        dataset_types = ['train', 'test', 'val']
        data_list = [
            get_sentence_data(
                graph_data,
                dataset_type
            )
            for dataset_type in dataset_types
            for graph_data in tqdm(self.graphs_data[dataset_type])
            if len(graph_data['edge_list']) > 2
        ]

        data, slices = self.collate(data_list)
        torch.save((data, slices), self.processed_paths[0])


if __name__ == '__main__':
    graphs_data = load_from_pickle(settings.GRAPH_DATA_PATH)
    graphs_data = train_val_test_split_graph_data(
        graphs_data, val_ratio=0.2, test_ratio=0.1
    )
    root = osp.join(
        'sentence2graph_gnn',
        'datasets',
        'semmed_sentence_split_dataset_simple'
    )
    dataset = SemMedSentenceSplitDataset(root=root, graphs_data=graphs_data)
