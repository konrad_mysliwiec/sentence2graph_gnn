import torch


def calculate_weights(labels: torch.tensor):
    if labels.shape[0] == 0:
        return None
    labels_ratio = torch.bincount(labels)/labels.shape[0]
    reversed_ratio = torch.tensor([1])/labels_ratio
    return reversed_ratio / reversed_ratio.sum()
