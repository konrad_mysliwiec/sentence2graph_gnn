import torch
import torch.nn.functional as F
import torch.nn as nn
import torch_geometric.nn as pyg_nn

from sentence2graph_gnn.models.model_args import ModelArgs


class NodeClassificationRGCNNetwork(nn.Module):
    def __init__(
        self,
        num_nodes: int,
        hidden_channels: int,
        num_relations: int,
        args: ModelArgs,
        output_dim: int,
        nodes_types: torch.Tensor,
        num_blocks: int = None
    ):
        super(NodeClassificationRGCNNetwork, self).__init__()
        self.base_node_emb = nn.Parameter(
            torch.Tensor(num_nodes, hidden_channels)
        )
        torch.nn.init.xavier_uniform_(self.base_node_emb)
        self.conv_layers = nn.ModuleList()
        self.conv_layers.append(
            pyg_nn.RGCNConv(
                in_channels=hidden_channels,
                out_channels=hidden_channels,
                num_relations=num_relations,
                aggr=args.aggr,
                num_blocks=num_blocks
            )
        )
        # self.norm_layers = nn.ModuleList()
        # self.norm_layers.append(nn.LayerNorm(hidden_channels))
        for _ in range(args.num_layers-1):
            self.conv_layers.append(
                pyg_nn.RGCNConv(
                    in_channels=hidden_channels,
                    out_channels=hidden_channels,
                    num_relations=num_relations,
                    aggr=args.aggr,
                    num_blocks=num_blocks
                )
            )
        for i in range(args.num_layers):
            self.conv_layers[i].reset_parameters()
            # self.norm_layers.append(nn.LayerNorm(hidden_channels))
        self.dropout = args.dropout
        self.num_layers = args.num_layers
        self.num_relations = num_relations
        self.nodes_types = nodes_types

        self.final_lin_layers = nn.Sequential(
            nn.Linear(hidden_channels, hidden_channels),
            nn.Dropout(args.dropout),
            nn.Linear(hidden_channels, output_dim)
        )

    def forward(self, edge_index, edge_type):
        x = self.base_node_emb[self.nodes_types]
        for layer in range(self.num_layers):
            x = self.conv_layers[layer](x, edge_index, edge_type)
            x = F.relu(x)
            x = F.dropout(x, p=self.dropout, training=self.training)

        x = self.final_lin_layers(x)
        out = F.log_softmax(x, dim=1)

        return out

    def loss(self, pred, label, weights=None):
        return F.nll_loss(pred, label, weight=weights)
