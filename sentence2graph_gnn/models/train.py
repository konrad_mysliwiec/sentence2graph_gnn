import os.path as osp
from sklearn.metrics import balanced_accuracy_score
from tqdm import tqdm
from tensorboardX import SummaryWriter
import torch
import torch.nn as nn
from torch_geometric.data import Dataset
from torch_geometric.loader import DataLoader

from sentence2graph_gnn.models.model_args import ModelArgs
from sentence2graph_gnn.models.node_class_model import \
    NodeClassificationRGCNNetwork
from sentence2graph_gnn.models.optimiser_util import build_optimizer
from sentence2graph_gnn.models.training_util import calculate_weights
from sentence2graph_gnn.utils import create_directory


def train(
    model: NodeClassificationRGCNNetwork,
    dataset: Dataset,
    args: ModelArgs,
    writer: SummaryWriter = None,
    checkpoint_dir: str = None
):
    train_loader = DataLoader(
        dataset[dataset.data.is_train],
        batch_size=args.batch_size,
        shuffle=False
    )
    test_loader = DataLoader(
        dataset[dataset.data.is_val],
        batch_size=args.batch_size,
        shuffle=False
    )
    _, opt = build_optimizer(args, model.parameters())

    for epoch in range(args.epochs):
        total_loss = 0
        model.train()
        preds = []
        labels = []
        for batch in tqdm(train_loader):
            opt.zero_grad()
            pred = model(batch.edge_index, batch.edge_type)
            pred = pred[batch.nodes_types]
            label = batch.y
            preds.append(pred)
            labels.append(label)
            weights = calculate_weights(label)
            loss = model.loss(pred, label, weights)
            loss.backward()
            opt.step()
            total_loss += loss.item() * batch.num_graphs
        total_loss /= len(train_loader.dataset)
        with torch.no_grad():
            preds = torch.cat(preds, dim=0).max(dim=1)[1]
            labels = torch.cat(labels, dim=0)
        train_accuracy = balanced_accuracy_score(labels, preds)
        if writer:
            writer.add_scalar("loss", total_loss, epoch)
            writer.add_scalar("train_accuracy", train_accuracy, epoch)
        print(total_loss)
        print(train_accuracy)

        if epoch % 10 == 0:
            test_checkpoint(
                test_loader=test_loader,
                model=model,
                opt=opt,
                epoch=epoch,
                checkpoint_dir=checkpoint_dir,
                writer=writer
            )


def test_checkpoint(
    test_loader: DataLoader,
    model: nn.Module,
    opt: torch.optim.Optimizer,
    epoch: int,
    checkpoint_dir: str,
    writer: SummaryWriter = None
):
    balanced_acc_score = test(test_loader, model)
    if writer:
        writer.add_scalar("test_accuracy_score", balanced_acc_score, epoch)
    print("Test balacned accuracy score: ", balanced_acc_score)
    if checkpoint_dir:
        create_directory(checkpoint_dir)
        checkpoint = {
            'model_state_dict': model.state_dict(),
            'optimizer_state_dict': opt.state_dict(),
            'epoch': epoch
        }
        checkpoint_file_path = osp.join(
            checkpoint_dir, f'checkpoint_{epoch}.pth'
        )
        torch.save(
            checkpoint,
            checkpoint_file_path
        )


def test(
    loader: DataLoader,
    model: nn.Module
) -> float:
    model.eval()
    preds = []
    labels = []
    for data in loader:
        with torch.no_grad():
            pred = model(data.edge_index, data.edge_type)
            pred = pred[data.nodes_types]
            label = data.y
            preds.append(pred)
            labels.append(label)
    preds = torch.cat(preds, dim=0).max(dim=1)[1]
    labels = torch.cat(labels, dim=0)
    return balanced_accuracy_score(labels, preds)
