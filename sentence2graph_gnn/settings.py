import os.path as osp


# LANGUAGE TAGS and DEPENDENCIES
RAW_TAGS_AND_DEPS_PATH = osp.join(
    "sentence2graph_gnn",
    "data",
    "tags_and_deps.py"
)

# ENCODERS
DEPENDENCIES_LABEL_ENCODER_PATH = osp.join(
    "sentence2graph_gnn",
    "data",
    "encoders",
    "label_encoders",
    "dependencies_encoder.pkl"
)
TAGS_LABEL_ENCODER_PATH = osp.join(
    "sentence2graph_gnn",
    "data",
    "encoders",
    "label_encoders",
    "tags_encoder.pkl"
)
DEPENDENCIES_ONE_HOT_ENCODER_PATH = osp.join(
    "sentence2graph_gnn",
    "data",
    "encoders",
    "one_hot_encoders",
    "dependencies_encoder.pkl"
)
TAGS_ONE_HOT_ENCODER_PATH = osp.join(
    "sentence2graph_gnn",
    "data",
    "encoders",
    "one_hot_encoders",
    "tags_encoder.pkl"
)

# RAW SEMMED DATA
# Original Semmed Sentence data
RAW_SEMMED_SENTENCE_DATA_PATH = osp.join(
    "sentence2graph_gnn",
    "data",
    "semmed",
    "raw",
    "SENTENCE.csv"
)
# Original Semmed Predication data
RAW_SEMMED_PREDICATION_DATA_PATH = osp.join(
    "sentence2graph_gnn",
    "data",
    "semmed",
    "raw",
    "PREDICATION.csv"
)
# Original Semmed Predication Aux data
RAW_SEMMED_PREDICATION_AUX_DATA_PATH = osp.join(
    "sentence2graph_gnn",
    "data",
    "semmed",
    "raw",
    "PREDICATION_AUX.csv"
)

# PROCESSED SEMMED DATA
# Merged Semmed data from: Predication, Predication Aux and Sentence tables
# This data contains sentences without a word being both subject and object
# Additionally it contains subject, object and predication indices
CLEAN_SEMMED_DATA_PATH = osp.join(
    "sentence2graph_gnn",
    "data",
    "semmed",
    "processed",
    "semmed_clean.csv"
)

# Semmed data with Subject, Predicate, Object
# This data is filtered to contain singular word subject, predicate or object
SVO_SEMMED_DATA_PATH = osp.join(
    "sentence2graph_gnn",
    "data",
    "semmed",
    "processed",
    "semmed_svo.pkl"
)

# GRAPH DATA
# List of dicitonaries with edges' types, edge index and edge labels.
GRAPH_DATA_PATH = osp.join(
    "sentence2graph_gnn",
    "data",
    "semmed",
    "processed",
    "graph_data.pkl"
)
