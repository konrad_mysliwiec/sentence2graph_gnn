import spacy

import sentence2graph_gnn.settings as settings
from sentence2graph_gnn.utils import load_from_pickle


nlp = spacy.load("en_core_web_md")
dep_label_encoder = load_from_pickle(
    settings.DEPENDENCIES_LABEL_ENCODER_PATH
)
tags_label_encoder = load_from_pickle(settings.TAGS_LABEL_ENCODER_PATH)


def parse_tree_with_edge_type_encoded(sentence: str) -> list:
    """Parses a sentence to an edge index (ids) of a dependencies graph.

    Parses a sentence to a graph and returns edge index with edge types.

    Args:
        sentence (str): Sentence to be parsed to an edge index with edge types.

    Returns:
        list: Edge index with edge type for a dependencies graph.
            Each element consists of list which contains three elements:
            source node id , target node id, edge type id.
            For example for a sentence 'Mike likes cats.' it will be:
            [
                [1, 0, 55],
                [1, 2, 30],
                [1, 3, 74]
            ]
            , where for each list first element (1) is a source node id,
            second elemend (0) is a target node id,
            thrid element (55) is an edge type id.
    """
    root = get_sentence_root(sentence)
    return parse_tree_with_edge_type_from_root_encoded(root)


def parse_tree_with_edge_type_from_root_encoded(
    root: spacy.tokens.token.Token
) -> list:
    """Parses dependencies tree to an edge index (ids) from a spacy token.

    Parses dependencies tree from spacy's root token to a graph and returns
        edge index with edge types.

    Args:
        root (spacy.tokens.token.Token): Root token of a sentence.

    Returns:
        list: Edge index with edge type for a dependencies graph.
            Each element consists of list which contains three elements:
            source node id , target node id, edge type id.
            For example for a sentence 'Mike likes cats.' it will be:
            [
                [1, 0, 55],
                [1, 2, 30],
                [1, 3, 74]
            ]
            , where for each list first element (1) is a source node id,
            second elemend (0) is a target node id,
            thrid element (55) is an edge type id.
    """
    edges = []
    if root.children:
        for child in root.children:
            edges.append([
                root.i,
                child.i,
                dep_label_encoder.transform([child.dep_])[0],
            ])
            edges.extend(parse_tree_with_edge_type_from_root_encoded(child))
    return edges


def parse_tree_with_edge_type(sentence: str) -> list:
    """Parses a sentence to an edge index (words) of a dependencies graph.

    Parses a sentence to a graph and returns edge index with edge types.
        Edge index in this case contains words rather than ids.

    Args:
        sentence (str): Sentence to be parsed to an edge index with edge types.

    Returns:
        list: Edge index with edge type for a dependencies graph.
            Each element consists of list which contains three elements:
            source node (word) , target node (word), edge type (dependency).
            For example for a sentence 'Mike likes cats.' it will be:
            [
                ['likes', 'Mike', 'nsubj'],
                ['likes', 'cats', 'dobj'],
                ['likes', '.', 'punct']
            ]
            , where first element ('subj') is a source node,
            second elemend ('pred') is a target node id,
            thrid element ('dep') is an edge type id.
    """
    root = get_sentence_root(sentence)
    return parse_tree_with_edge_type_from_root(root)


def parse_tree_with_edge_type_from_root(
    root: spacy.tokens.token.Token
) -> list:
    """Parses dependencies tree to an edge index (words) from a spacy token.

    Parses dependencies tree from spacy's root token to a graph and returns
        edge index with edge types. Nodes ids and edge types are not encoded,
        which means that index in this case contains words rather than ids.

    Args:
        root (spacy.tokens.token.Token): Root token of a sentence.

    Returns:
        list: Edge index with edge type for a dependencies graph.
            Each element consists of list which contains three elements:
            source node (word) , target node (word), edge type (dependency).
            For example for a sentence 'Mike likes cats.' it will be:
            [
                ['likes', 'Mike', 'nsubj'],
                ['likes', 'cats', 'dobj'],
                ['likes', '.', 'punct']
            ]
            , where first element ('subj') is a source node,
            second elemend ('pred') is a target node id,
            thrid element ('dep') is an edge type id.
    """
    edges = []
    if root.children:
        for child in root.children:
            edges.append([
                root.text,
                child.text,
                child.dep_,
            ])
            edges.extend(parse_tree_with_edge_type_from_root(child))
    return edges


def get_sentence_root(sentence: str) -> spacy.tokens.token.Token:
    """Finds and returns root token of a sentence.

    The returned token is a spacy token.

    Args:
        sentence (str): Sentence to return token for.

    Returns:
        spacy.tokens.token.Token: Root token of a sentence in a form of
            spacy token.
    """
    doc = nlp(sentence)
    return [token for token in doc if token.head == token][0]


def get_nodes_features_labels(sentence: str) -> list:
    """Returns encoded list of tags for each word in a sentence.

    These tags can serve as node features ids for embeddings.

    Args:
        sentence (str): Sentence to retrieve words' tags ids for.

    Returns:
        list: List with words tags ids. The order of the elements
            preserves the order of words appearing in a sentence.
    """
    doc = nlp(sentence)
    nodes_features = []
    for token in doc:
        nodes_features.append(
            tags_label_encoder.transform([token.tag_])[0]
        )
    return nodes_features


def get_edge_index_from_root(root: spacy.tokens.token.Token) -> list:
    """Parses dependencies tree to an edge index from a spacy token.

    Args:
        root (spacy.tokens.token.Token): Root token of a sentence.

    Returns:
        list: Edge index of a dependencies graph.
    """
    edges = []
    if root.children:
        for child in root.children:
            edges.append([
                root.i,
                child.i
            ])
            edges.extend(get_edge_index_from_root(child))
    return edges


def get_edge_index(sentence: str) -> list:
    """Parses a sentence (string) to an edge index of a dependencies graph.

    Args:
        sentence (str): Sentence to be parsed to an edge index.

    Returns:
        list: Edge index of a dependencies graph.
    """
    root = get_sentence_root(sentence)
    return get_edge_index_from_root(root)
