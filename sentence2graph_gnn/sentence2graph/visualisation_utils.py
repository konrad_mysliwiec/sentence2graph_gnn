import networkx as nx
import matplotlib.pyplot as plt

from sentence2graph_gnn.sentence2graph.sentence_to_graph import \
    get_nodes_features_labels, parse_tree_with_edge_type


def draw_sentence_graph(sentence: str):
    """Draws a graph from sentence.

    In this graph tokens are nodes, where each token tag is a node type/colour.
    Edges are dependencies between tokens from parse tree.

    Args:
        sentence (str): A sentence to be drawn as a graph.
    """
    nodes_features = get_nodes_features_labels(sentence)
    edge_data = parse_tree_with_edge_type(sentence)
    edge_index = [[edge[0], edge[1]] for edge in edge_data]
    edge_type = [edge[2] for edge in edge_data]
    del edge_data

    tree = nx.DiGraph()
    for i, edge_label in enumerate(edge_type):
        source_node = edge_index[i][0]
        target_node = edge_index[i][1]
        tree.add_edge(source_node, target_node, dep=edge_label)

    node_color = [label/max(nodes_features) for label in nodes_features]

    plt.figure(figsize=(10, 10))
    pos = nx.spring_layout(tree)
    nx.draw(
        tree, pos, with_labels=True, node_color=node_color, cmap=plt.cm.jet
    )

    edge_labels = nx.get_edge_attributes(tree, 'dep')
    formatted_edge_labels = {
        (elem[0], elem[1]): edge_labels[elem]
        for elem in edge_labels
    }
    nx.draw_networkx_edge_labels(
        tree, pos=pos, edge_labels=formatted_edge_labels, font_color='red'
    )
    plt.show()


if __name__ == '__main__':
    draw_sentence_graph("Mike likes cats.")
