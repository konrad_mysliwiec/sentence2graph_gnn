# sentence2graph

## Repo structure

### SemMed ETL

All scripts for SemMed processing can be found in 'sentence2graph_gnn/semmed_processing' directory.
The scripts below serves for SemMed ETL purposes and should be run in the provided order:

    - clean_semmed_processing.py
    - svo_semmed_processing.py
    - semmed_sentence_to_graph.py

At the end of this pipeline data are stored in a graph data dictionary format in directory specified by 'GRAPH_DATA_PATH' variable in settings.py

### Sentence to graph

Scripts for parsing sentences to graphs:

    - sentence_to_graph.py
    - visualisation_utils.py

These scripts can be found in 'sentence2graph_gnn/sentence2graph' directory.

### Utility scripts

Scripts with general purposes

    - encoders.py
    - settings.py
    - utils.py

These scripts can be found in 'sentence2graph_gnn' directory.

### TO DO

    - Create GraphData class instead of passing dictionary with graph data.
    - Add model name, checkpoint_dir and SummaryWriter to ModelArgs class
    - Introduce Neptune.ai for models tracking, models' metadata and artifacts
    - Implement FastRGCN
